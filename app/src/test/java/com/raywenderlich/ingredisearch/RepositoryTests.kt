package com.raywenderlich.ingredisearch

import android.content.SharedPreferences
import com.google.gson.Gson
import com.mrempher.ingrediantSearcher.Recipe
import com.mrempher.ingrediantSearcher.RecipeRepository
import com.mrempher.ingrediantSearcher.RecipeRepositoryImpl
import com.nhaarman.mockito_kotlin.*
import org.junit.Before
import org.junit.Test

class RepositoryTests {

    private lateinit var spyRespository: RecipeRepository
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var sharedPreferenceEditor: SharedPreferences.Editor

    @Before
    fun setup() {
        sharedPreferences = mock()
        sharedPreferenceEditor = mock()
        whenever(sharedPreferences.edit()).thenReturn(sharedPreferenceEditor)

        spyRespository = spy(RecipeRepositoryImpl(sharedPreferences))
    }

    @Test
    fun addFavorite_withEmptyRecipes_savesJsonRecipe() {
        doReturn(emptyList<Recipe>()).whenever(spyRespository).getFavoriteRecipes()

        val recipe = Recipe("id", "title", "imageUrl",
                "sourceUrl", false)
        spyRespository.addFavorite(recipe)

        inOrder(sharedPreferenceEditor) {
            val jsonString = Gson().toJson(listOf(recipe))
            verify(sharedPreferenceEditor).putString(any(), eq(jsonString))
            verify(sharedPreferenceEditor).apply()
        }
    }
}