package com.raywenderlich.ingredisearch

import com.mrempher.ingrediantSearcher.SearchPresenter
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString

class SearchTests {

    private lateinit var presenter: SearchPresenter
    private lateinit var view: SearchPresenter.View

    @Before
    fun setup() {
        presenter = SearchPresenter()
        view = mock()
        presenter.attachView(view)
    }

    @Test
    fun search_withEmptyQuery_callShowQueryRequiredMessage() {
        presenter.search("")
        verify(view).showQueryRequiredMessage()
    }

    @Test
    fun search_withEmptyQuery_doesNotCallShowSearchResults() {
        presenter.search("")
        verify(view, never()).showSearchResults(anyString())
    }


}